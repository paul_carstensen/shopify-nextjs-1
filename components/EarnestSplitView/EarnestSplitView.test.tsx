/** @jest-environment jsdom */
import { render } from '@testing-library/react';
import { EarnestSplitView, EarnestSplitViewProps } from '.';

const renderText = ({ title, subtitle, backgroundImageUrl }: EarnestSplitViewProps) => (
  render(
    <EarnestSplitView
      title={title}
      subtitle={subtitle}
      backgroundImageUrl={backgroundImageUrl}
    />,
  )
);

describe('Earnest Split View component', () => {
  const title = 'Earnest App';
  const subtitle = 'This text need to be tested.';
  const backgroundImageUrl = '/background.png';

  it('should render without crashing', () => {
    expect(() => {
      renderText({ title, subtitle, backgroundImageUrl });
    }).not.toThrowError();
  });

  it('should render the text', () => {
    const earnestSplitViewComponent = renderText({ title, subtitle, backgroundImageUrl });

    expect(earnestSplitViewComponent.getByText(title)).toBeInTheDocument();
    expect(earnestSplitViewComponent.getByText(subtitle)).toBeInTheDocument();
  });
});
