import React, { MouseEventHandler, useCallback, useState } from 'react';
import * as Yup from 'yup';
import Image from 'next/image';
import { useDropzone } from 'react-dropzone';
import { useMediaQuery } from 'react-responsive';
import { useFormik } from 'formik';
import { RoundedButtonLinkDark } from '../../../common/Buttons';
import uploadIcon from '../../../../assets/images/xmas-gimmick/upload.png';
import readFileAsDataURL from '../../../../helpers/readFileAsDataURL';
import { faceSwap, Genders } from '../../../../helpers/nachhaltigkeit/weihnachtselfen/api';
import RadioInput from './RadioInput';
import {
  GenderRadioButtonsContainer,
  ImageUploadContent,
  RadioButtonsGroup,
  SubTitle,
  Title,
  UnderlinedText,
  UploadInput,
  UploadInputWrapper,
  UploadLabel,
} from './styled';
import { XmasFirstScreenProps } from '../types';
import { ErrorDialog } from '../ErrorDialog';
import { LoadingDialog } from '../LoadingDialog';
import { ConsentTextComponent } from './ConsentTextComponent';

export const ImageUploadComponent: React.FC<XmasFirstScreenProps> = ({ setFaceSwapResult }) => {
  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error | null>(null);

  const { setFieldValue, values, handleSubmit } = useFormik<{
    image: string,
    gender: Genders | null,
  }>({
    initialValues: {
      image: '',
      gender: null,
    },
    validationSchema: Yup.object().shape({
      image: Yup.string().required(),
      gender: Yup.string().nullable(),
    }),
    onSubmit: async (v) => {
      try {
        setLoading(true);
        setError(null);
        const result = await faceSwap(v);
        setLoading(false);
        setFaceSwapResult(result);
      } catch (err: unknown) {
        setLoading(false);
        setError(err instanceof Error ? err : new Error('Unbekannter Fehler'));
      }
    },
  });

  const onDrop = useCallback(async (acceptedFiles) => {
    const file = acceptedFiles[0];
    const base64 = await readFileAsDataURL(file, 0.4, 1000, 1000);

    setError(null);
    setFieldValue('image', base64);
  }, [setFieldValue]);

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  const renderDropZoneContent = () => {
    if (error) return <ErrorDialog />;

    return (
      <UploadLabel>
        {
            !values.image
              ? (
                <>
                  <Image
                    data-testid="upload-icon"
                    src={uploadIcon}
                    alt="Upload-Symbol"
                    height={60}
                    width={60}
                  />
                  {
                    !isMobile
                      ? (
                        <p>
                          Dateien per Drag & Drop hierher ziehen oder
                        </p>
                      )
                      : null
                  }
                  <UnderlinedText>
                    Bild auswählen
                  </UnderlinedText>
                </>
              )
              : null
          }
      </UploadLabel>
    );
  };

  return (
    <ImageUploadContent>
      <Title>jetzt dein bild hochladen</Title>
      <SubTitle><ConsentTextComponent /></SubTitle>
      {loading
        ? <LoadingDialog />
        : (
          <>
            <UploadInputWrapper
              data-testid="dropzone"
              {...getRootProps({
                backgroundImage: values.image,
              })}
            >
              <UploadInput
                type="file"
                id="img"
                {...getInputProps({
                  accept: '.png,.jpg,.jpeg',
                  multiple: false,
                })}
              />
              {renderDropZoneContent()}
            </UploadInputWrapper>
            <GenderRadioButtonsContainer>
              <RadioButtonsGroup>
                <RadioInput
                  value={Genders.FEMALE}
                  label="Weiblich"
                  onChange={(value) => {
                    setFieldValue('gender', value);
                  }}
                  checked={values.gender === Genders.FEMALE}
                />
                <RadioInput
                  value={Genders.MALE}
                  label="Männlich"
                  onChange={(value) => {
                    setFieldValue('gender', value);
                  }}
                  checked={values.gender === Genders.MALE}
                />
                <RadioInput
                  value={null}
                  label="Keine Angabe"
                  onChange={(value) => {
                    setFieldValue('gender', value);
                  }}
                  checked={values.gender === null}
                />
              </RadioButtonsGroup>
            </GenderRadioButtonsContainer>
            <RoundedButtonLinkDark
              role="button"
              onClick={handleSubmit as unknown as MouseEventHandler}
              data-tracking-id={values.image && 'ga-weihnachtselfen-lets-go'}
            >
              Los geht&rsquo;s!
            </RoundedButtonLinkDark>
          </>
        )}
    </ImageUploadContent>
  );
};
