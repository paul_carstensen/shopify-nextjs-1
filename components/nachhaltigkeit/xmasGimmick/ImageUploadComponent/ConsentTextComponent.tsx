import React, { useState } from 'react';
import styled from 'styled-components';

type ShowMoreButtonProps = {
  showingMoreConsent: boolean;
}

const ShowMoreButton = styled.button<ShowMoreButtonProps>`
  margin-top: ${(props) => (props.showingMoreConsent ? '5px' : 'auto')};
`;

export const ConsentTextComponent: React.FC = () => {
  const [showingMoreConsent, setshowingMoreConsent] = useState(false);

  return (
    <>
      <span>
        Wenn Du auf den Button &ldquo;Los geht&apos;s!&rdquo; klickst, wird Dein Foto verschlüsselt in unsere Google Drive*-Cloud übertragen, von unserer Software in einen anderen Hintergrund eingefügt (&ldquo;face swap&rdquo;) und das neue Bild (temporär) gespeichert.<br />
      </span>
      { showingMoreConsent && (
      <span>
        Bei &ldquo;Download&rdquo; wird Dein neues Bild von uns nur für die Dauer Deines Besuchs dieser Webpage gespeichert und danach automatisch gelöscht.<br />
        Du kannst Dein neues Bild mit anderen teilen. Beachte bitte, dass Du das Bild aus lizenzrechtlichen Gründen nur als Ganzes teilen darfst, also nur so, wie Du es von unserer Website herunterladen oder teilen kannst.<br />
        Für weitere Informationen, auch zu Deinen Datenschutzrechten, s. unsere Datenschutzerklärung.<br />
        *Anbieter von Google Drive ist das Unternehmen Google LLC, 1600 Amphitheatre Parkway Mountain View, CA 94043, USA, d.h. Dein Foto/Bild kann in den USA, einem Drittland mit unzureichendem Datenschutzniveau gespeichert werden, in dem insbesondere das Risiko besteht, dass die Daten durch staatliche US-Stellen zu (geheimdienstlichen) Kontroll- und Überwachungszwecken verarbeitet werden, ohne dass wir oder Du dies mitbekommst und ohne dass Dir dagegen stets hinreichende Rechtsbehelfe zustehen.<br />
      </span>
      ) }
      <ShowMoreButton type="button" onClick={() => setshowingMoreConsent(!showingMoreConsent)} showingMoreConsent={showingMoreConsent}>
        <strong>{ showingMoreConsent ? 'Weniger anzeigen' : 'Mehr anzeigen'}</strong>
      </ShowMoreButton>
    </>
  );
};
