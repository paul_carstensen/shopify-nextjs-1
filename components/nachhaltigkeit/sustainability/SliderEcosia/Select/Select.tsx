import React, { useState } from 'react';
import styled from 'styled-components';

const SelectStyled = styled.select`
  align-self: center;
  background-color: #e6e6e6;
  padding: 15px;
  margin-right: 10px;
  border: none;
  padding-right: 25px;
  line-height: 1.6;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background-position: right center;
  background-image: url(//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/ico-select.svg);
  background-repeat: no-repeat;
  background-position: right 10px center;
  background-size: 11px;
  text-indent: 0.01px;
  text-overflow: "";
  cursor: pointer;
  color: inherit;
`;

const SelectOption = styled.option`
`;

export type Option = {
  title: string;
  value: number | string;
}

export type SelectQuestionProps = {
  value: number | string;
  options: Option[];
  onSelectChange?: (event?: any) => void;
}

const renderOption = ({ title, value }: Option) => (
  <SelectOption
    key={title}
    value={value}
  >
    {title}
  </SelectOption>
);

export const Select = ({ value, options, onSelectChange }: SelectQuestionProps) => {
  const [stateValue, setStateValue] = useState<number | string>(value);
  const select = (
    <SelectStyled
      onChange={(event) => {
        setStateValue(event.target.value);
        onSelectChange?.(event);
      }}
      value={stateValue}
    >
      {options.map(renderOption)}
    </SelectStyled>
  );
  return (
    <>
      {select}
    </>
  );
};
