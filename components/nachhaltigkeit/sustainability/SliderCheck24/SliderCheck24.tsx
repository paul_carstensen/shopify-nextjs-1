import React, { useState } from 'react';
import { Button } from '@u2dv/marketplace_ui_kit/dist/components';
import styled from 'styled-components';
import { SliderContainer } from '../../../SliderContainer';
import watermark from '../../../../assets/images/sustainability/watermark-slide.png';

export interface SliderCheck24Props {
  nextTipp: any,
}

const StyledButton = styled.span`
  display: inline-block;
  margin-right: 20px;
  
  & a:hover {
    color: white;
  }

  @media screen and (max-width: 768px) {
    margin-bottom: 20px;
  }
`;

const StyledContainerButton = styled.div`
  margin: 20px 0;
`;

export const SliderCheck24: React.FC<SliderCheck24Props> = ({ nextTipp }) => {
  const [isClicked, setIsClicked] = useState<boolean>(false);

  const goToCheck24 = () => {
    setIsClicked(true);
  };

  return (
    <SliderContainer
      counter="4"
      numSlides="5"
      backgroundColor="#98b6a0"
      backgroundImage={watermark}
      watermarkBg
      title="100% Ökostrom?"
      text="Fast 70% der deutschen Haushalte beziehen Strom bei ihrem Grundversorger und unterstützen damit oft unwissentlich Kohlekraftwerke & co. Dabei ist wechseln heute so leicht. Probier’s aus!"
    > {!isClicked ? (
      <StyledContainerButton>
        <StyledButton>
          <Button
            as="a"
            href="https://www.awin1.com/cread.php?awinmid=9364&awinaffid=767925&ued=https%3A%2F%2Fwww.check24.de%2Fstrom%2Foekostrom%2F"
            target="_blank"
            rel="noopener noreferrer"
            data-testid="btn-check24-continue"
            trackingId="ga-sust-for-beginners-check24-continue"
            onClick={goToCheck24}
          >Los geht&apos;s
          </Button>
        </StyledButton>
        <Button appearance="secondary" data-testid="btn-check24-skip" trackingId="ga-sust-for-beginners-check24-skip" onClick={nextTipp}> Nein, lieber ein anderer Tipp</Button>
      </StyledContainerButton>
    ) : <Button trackingId="ga-sust-for-beginners-check24-finish" onClick={nextTipp}> Fertig</Button>}
    </SliderContainer>
  );
};
