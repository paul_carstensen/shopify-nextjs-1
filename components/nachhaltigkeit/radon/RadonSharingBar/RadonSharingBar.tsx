import React from 'react';
import { SharingBar } from '../../../SharingBar';

export const RadonSharingBar: React.FC = () => {
  const url = 'https://www.uptodate.de/nachhaltigkeit/radon';
  const message = 'Radioaktives Radon in Deutschland - informiere Dich hier: {url}'; // It's not a mistake
  const emailSubject = 'Radioaktives Radon in Deutschland - informiere Dich jetzt';
  const emailBody = `Jede/r Hausbesitzer:in sollte sich der potenziellen Gefahr einer Radon-Belastung im eigenen Keller bewusst sein. Ob Privat-, Firmen- oder öffentlicher Grund - das geruchlose Gas ist in manchen Gegenden Deutschlands stark konzentriert.

Doch was ist Radon? Es handelt sich um ein natürliches radioaktives Element, das in den Zerfallsreihen des Urans und Thoriums vorkommt. Dieses Gas, das meistens aus dem Untergrund in Häuser eindringt, kann unter ungünstigen Bedingungen die Aktivität der Raumluft so stark steigen lassen, dass das Risiko, an Lungenkrebs zu erkranken, nennenswert steigt.

Aufgrund der Geruchlosigkeit kann die konkrete Belastung nur durch eine spezielle Messung im Haus festgestellt werden.

Ob Deine Gegend einem erhöhten Risiko unterliegt, kannst Du vorab hier prüfen: {url}`;

  return (
    <SharingBar
      urls={url}
      message={message}
      emailSubject={emailSubject}
      emailBody={emailBody}
      ctaText="Jetzt teilen"
    />
  );
};
