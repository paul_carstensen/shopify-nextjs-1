import React from 'react';
import { EarnestSplitView } from '../../../EarnestSplitView';
import earnestSplitViewBgImage from '../../../../assets/images/earnest/earnest-split-view-bg.jpg';

export const RadonEarnestSplitView: React.FC = () => (
  <EarnestSplitView
    title="Earnest App"
    subtitle="Earnest zeigt Dir, wie Du mit kleinen Schritten Großes bewegst und lässt Dir dabei die individuelle Freiheit, Dein Leben so nachhaltig zu führen, wie Du es willst."
    earnestText="Die Earnest App"
    appStoreCtaUrl="https://apps.apple.com/app/apple-store/id1515761747?mt=8"
    playStoreCtaUrl="https://play.google.com/store/apps/details?id=de.uptodate.earnest.app"
    imagePosition="right"
    backgroundImageUrl={earnestSplitViewBgImage.src}
  />
);
