import React from 'react';
import styled from 'styled-components';
import Hero from '../../../Hero';
import { RadonSharingBar } from '../RadonSharingBar';
import { ArabeskForm } from '../ArabeskForm';
import hero360x from '../../../../assets/images/radon/gimmick-hero_360x.jpg';
import hero540x from '../../../../assets/images/radon/gimmick-hero_540x.jpg';
import hero1420x from '../../../../assets/images/radon/gimmick-hero_1420x.jpg';
import hero2840x from '../../../../assets/images/radon/gimmick-hero_2840x.jpg';
import { MainContainerWithElementStyles } from '../../../common/MainContainer';

const ArabeskFormContainer = styled.div`
  text-align: center;
  margin: 50px 0 20px 0;
`;

const SharingBarContainer = styled.div`
  text-align: center;
`;

export type ArabeskPageProps = {
  onSubmit: (arabeskData: any) => void;
}

export const ArabeskPage: React.FC<ArabeskPageProps> = ({ onSubmit }) => (
  <MainContainerWithElementStyles>
    <Hero
      title="Checkliste"
      subtitle="Bitte beantworte die folgenden Fragen um auf Deine persönlichen Risiken zugeschnittene Empfehlungen zu erhalten."
      backgroundImages={[hero360x.src, hero540x.src, hero1420x.src, hero2840x.src]}
      resolutions={[360, 540, 1420, 2840]}
    />
    <SharingBarContainer>
      <RadonSharingBar />
    </SharingBarContainer>
    <ArabeskFormContainer>
      <ArabeskForm onSubmit={onSubmit} />
    </ArabeskFormContainer>
  </MainContainerWithElementStyles>
);
