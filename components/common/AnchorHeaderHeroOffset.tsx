import styled from 'styled-components';

/**
 * Block adding an offset for hero component to prevent derivation added by header (-16px on in desktop)
 * @todo Fix header to remove this component
 */
export const AnchorHeaderHeroOffset = styled.div`
  @media screen and (min-width: 769px) {
    &:before {
      display: block;
      content: " ";
      height: 16px;
      visibility: hidden;
    }
  }
`;
