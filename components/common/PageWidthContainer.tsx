import styled from 'styled-components';

export const PageWidthContainer = styled.div`
  max-width: 1440px;
  margin: auto;
  padding: 0px 40px 40px 40px;

  @media only screen and (max-width: 768px) {
    padding: 0px 20px 20px 20px;
  }
`;
