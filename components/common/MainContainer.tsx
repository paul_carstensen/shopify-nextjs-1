import styled from 'styled-components';

/**
 * Container unsetting the box sizing for most elements
 */
export const MainContainer = styled.div`
  font-family: Quicksand;
  width: 100%;
  margin: auto;

  @media (min-width: 1440px) {
    margin: auto;
  }

  & *,
  & input,
  & :before,
  & :after {
    box-sizing: unset;
  }
`;

/**
 * Container setting style of HTML elements like p, h1 etc... and exposing css classes
 * @note this container also unset box sizing
 * @warning avoid this component at all cost! It makes inner styling behaving improperly and you use dedicated heading/paragraph components!
 */
export const MainContainerWithElementStyles = styled(MainContainer)`
  & h1, & h2, & h3, & h4, & h5, & h6, & .h1, & .h2, & .h3, & .h4, & .h5, & .h6 {
    margin-bottom: 0.5rem;
    font-weight: 500;
    line-height: 1.2;
  }

  & h1, & .h1 {
    font-size: 2.5rem;
  }

  & h2, & .h2 {
    font-size: 2rem;
  }

  & h3, & .h3 {
    font-size: 1.75rem;
  }

  & h4, & .h4 {
    font-size: 1.5rem;
  }

  & h5, & .h5 {
    font-size: 1.25rem;
  }

  & h6, & .h6 {
    font-size: 1rem;
  }

  & h1, & h2 {
    padding-top: 15px;
    padding-bottom: 15px;
    font-family: Quicksand;
    font-size: 32px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
  }

  & .btn {
    padding: 11px 20px;
    display: inline-block;
  }

  @media only screen and (max-width: 768px) {
    & h1, & h2 {
      font-size: 24px;
    }

    & h3 {
      font-size: 18px;
    }
  }
`;
