import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Button } from '@u2dv/marketplace_ui_kit/dist/components';
import { SanitizedHTML } from '../../../SanitizedHTML';
import { PolicyModal } from '../../../PolicyModal';
import { FormErrorText } from '../../../FormErrorText';
import { KeepMeUpdatedBody } from '../../../../types/risk/lifestyle';
import { keepMeUpdated } from '../../../../helpers/lifeStyle/api';

type RiskDataConsentProps = {
  title: string;
  policyTextHtml: string;
}

const RiskDataConsentContainer = styled.div`
  max-width: 400px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: Poppins;
  align-items: center;
  padding: 0px 44px;
`;

const Title = styled.div`
  font-size: 20px;
  line-height: 24px;
  font-weight: 600;
  text-align: center;
  padding-bottom: 15px;
  font-weight: bold;
`;

const SubTitle = styled.div`
  text-align: center;
  font-size: 14px;
  padding-bottom: 10px;
`;

const ConsentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 25px;
  margin-bottom: 18px;
  padding-top: 25px;
`;

const ConsentComponent = styled.div`
  grid-area: consent;
  & label div {
    display: inline-block;
    width: 200px;
    vertical-align: text-top;
    letter-spacing: 0em;
    text-transform: none;
    overflow: visible;
    text-overflow: ellipsis;
    white-space: normal;
    text-align: left;
  }
  & label div :first-child {
    display: inline;
  }
  & label div :not(:first-child) {
    display: none;
  }
  & button {
    margin: -8px 0 10px 0px;
    position: absolute;
    font-weight: bold;
    font-size: 12px;
  }
`;

const Label = styled.label`
  text-transform: uppercase;
  letter-spacing: 0.3em;
  font-size: 0.75em;
  display: block;
  margin-bottom: 10px;

  &[for] {
    cursor: pointer;
  }

  &.error {
    color: #d02e2e;
  }
`;

const InputField = styled.input`
  border: 1px solid #e6e6e6;
  max-width: 100%;
  padding: 8px 10px;
  margin-bottom: 25px;
  border-radius: 25px;
  border: none;
  box-shadow: 5px 10px 30px 0 rgba(178, 178, 178, 0.25);
  font-family: 'Quicksand';
  color: #19232d;
  font-size: 12px;

  &.error {
    border-color: #d02e2e;
    background-color: #fff6f6;
    color: #d02e2e;
  }
`;

const CONSENT_GTM_NAME = 'Google Analytics';

const usercentricsLoaded = (): boolean => (
  // Usercentrics SDK will not be loaded during SSR or in development mode
  typeof window?.UC_UI?.getServicesBaseInfo === 'function'
);

type ConsentObject = {
  name: string;
  consent: {
    status: boolean;
  };
  id: string;
}

const getConsentObject = (vendorName: string): ConsentObject | undefined => {
  if (!usercentricsLoaded()) {
    return undefined;
  }

  const consents = window.UC_UI.getServicesBaseInfo() as ConsentObject[];

  return consents.find(({ name }) => name === vendorName);
};

const getConsentStatus = (vendorName: string): boolean => {
  const { consent } = getConsentObject(vendorName) || { consent: { status: false } };

  return consent.status;
};

const UpdatedButton = styled(Button)<{ disabled: boolean }>`
  cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};
  pointer-events: ${(props) => (props.disabled ? 'none' : 'all')};
`;

export const RiskDataConsent: React.FC<RiskDataConsentProps> = ({
  title, policyTextHtml,
}) => {
  const [email, setEmail] = useState('');
  const [hasPolicyConsent, setHasPolicyConsent] = useState(false);
  const [hasConsent, setHasConsent] = useState(false);
  const [policyModalOpen, setPolicyModalOpen] = useState(false);
  const [emailValidation, setEmailValidation] = useState('');

  const handleUserCentricsInitialized = () => {
    setHasConsent(getConsentStatus(CONSENT_GTM_NAME));
  };

  useEffect(() => {
    window?.addEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);

    return () => {
      window?.removeEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);
    };
  }, []);

  const submitRiskData = async (data: KeepMeUpdatedBody) => {
    await keepMeUpdated(data);
  };

  const onButtonClick = () => {
    if (hasConsent) {
      const riskData = localStorage.getItem('riskData');
      const riskDataJson: KeepMeUpdatedBody = JSON.parse(riskData || '');
      if (hasPolicyConsent) {
        if (email && emailValidation.length === 0) {
          const updatedRiskDataJson = {
            ...riskDataJson,
            email,
          };
          localStorage.setItem('riskData', JSON.stringify(updatedRiskDataJson));
          submitRiskData(updatedRiskDataJson);
        } else if (email.length === 0) {
          submitRiskData(riskDataJson);
        }
      }
    }
  };

  const onBlur = () => {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (emailPattern.test(email) || email.length === 0) {
      setEmailValidation('');
    } else {
      setEmailValidation('Ungültige E-Mail-Adresse');
    }
  };

  return (
    <RiskDataConsentContainer>
      <Title>{title}</Title>
      <SubTitle>We extend our tips daily but unfortunately we don’t have right tips for your risks. </SubTitle>
      <SubTitle>If you give us your email, we will inform you as soon as we have something nice for you!</SubTitle>

      <ConsentWrapper>
        <PolicyModal
          isOpen={policyModalOpen}
          closeModal={() => setPolicyModalOpen(false)}
          policyTextHtml={policyTextHtml}
        />
        <InputField
          id="email"
          name="email"
          type="text"
          placeholder="Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          onBlur={onBlur}
        />
        {emailValidation && <FormErrorText>{emailValidation}</FormErrorText>}
        <ConsentComponent>
          <Label htmlFor="consent">
            <input
              id="consent"
              type="checkbox"
              name="consent"
              onChange={(e) => setHasPolicyConsent(e.target.checked)}
            />
            <SanitizedHTML innerHTML={policyTextHtml} />
          </Label>
          <button type="button" onClick={() => setPolicyModalOpen(true)}>Mehr anzeigen</button>
        </ConsentComponent>
      </ConsentWrapper>
      <UpdatedButton
        trackingId="turbulanz-risktip-subscribe"
        appearance="primary"
        onClick={onButtonClick}
        disabled={!hasPolicyConsent && emailValidation.length > 0}
      >
        Keep me updated
      </UpdatedButton>
    </RiskDataConsentContainer>
  );
};
