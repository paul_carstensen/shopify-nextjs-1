import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import blogTimeImage from '../../../../assets/images/blogs/blog-time.png';

const TimeItem = styled.div`
  font-size: 18px;
  line-height: 1.33;
  padding-right: 10px;
  img {
    padding-right: 10px !important;
  }
`;

type BlogTimeProps = {
  time: number;
}

export const BlogTime: React.FC<BlogTimeProps> = ({ time }) => (
  <TimeItem>
    | <Image
      src={blogTimeImage}
      alt="Uhrsymbol"
      width="14"
      height="14"
    />{time} min
  </TimeItem>
);
