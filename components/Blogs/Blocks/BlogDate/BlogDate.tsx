import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import blogDateImage from '../../../../assets/images/blogs/blog-date.png';

const Date = styled.div`
  font-size: 18px;
  line-height: 1.33;
  padding-right: 10px;
  img {
    padding-right: 10px !important;
  }
`;
const months = ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
  'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];

const formatDate = (date: string) => {
  const dt = date.split('T');
  const dateArray = dt[0].split('-');
  const monthName = months[parseInt(dateArray[1], 10) - 1];
  return `${dateArray[2]}. ${monthName} ${dateArray[0]}`;
};

type BlogDateProps = {
  date: string;
}

export const BlogDate: React.FC<BlogDateProps> = ({ date }) => (
  <Date>
    <Image
      src={blogDateImage}
      alt="Kalendersymbol"
      width="14"
      height="14"
    />{formatDate(date)}
  </Date>
);
