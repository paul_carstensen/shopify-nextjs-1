import { categoryKeys, InterestKey, interestKeys } from 'helpers/homePageConfig';
import { isUserPreference } from './userPreferences';

describe('isUserPreference', () => {
  it('should be false on nullish values', () => {
    expect(isUserPreference(undefined)).toBe(false);
    expect(isUserPreference(null)).toBe(false);
  });

  it('should be true for valid UserPreference', () => {
    expect(isUserPreference({ categories: categoryKeys, interest: interestKeys[0] })).toBe(true);
  });

  it('should be false for empty categories', () => {
    expect(isUserPreference({ categories: [], interest: 'apps' as InterestKey })).toBe(false);
  });

  it('should be false for missing categories', () => {
    expect(isUserPreference({ interest: 'apps' as InterestKey })).toBe(false);
  });

  it('should be false for missing interest', () => {
    expect(isUserPreference({ categories: categoryKeys[0] })).toBe(false);
  });

  it('should be false for wrong interest', () => {
    expect(isUserPreference({ categories: categoryKeys[0], interest: 'hullebulle' })).toBe(false);
  });

  it('should be false for wrong category', () => {
    expect(isUserPreference({ categories: ['hullebulle'], interest: interestKeys[0] })).toBe(false);
  });
});
