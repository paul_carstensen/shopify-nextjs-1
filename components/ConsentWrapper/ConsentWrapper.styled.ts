import styled from 'styled-components';

interface ConsentOuterWrapperProps {
  hasConsent: boolean
}

interface CustomSwitchWrapperProps {
  isChecked: boolean
}

export const ConsentOuterWrapper = styled.div<ConsentOuterWrapperProps>`
  max-width: 100%;
  margin-bottom: 15px;
  padding: 0 20px 20px 20px;

  background-image: ${({ hasConsent }) => (hasConsent
    ? 'none'
    : "url('https://cdn.shopify.com/s/files/1/0268/4819/8771/files/consent_final_pic.png?v=1614786439')")};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  @media screen and (max-width: 731px) and (min-width: 340px) {
    background-position: center 65%;
  }
  @media screen and (max-width: 732px) {
    background-image: url('https://cdn.shopify.com/s/files/1/0268/4819/8771/files/Group_77.png?v=1614696698');
    padding-left: 0;
    padding-right: 0;
  }
`;

export const VideoPlaceholderWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const YoutubeWallWrapper = styled.div`
  @media screen and (max-width: 732px) {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

export const WallText = styled.div`
  width: 40%;

  @media screen and (max-width: 732px) {
    width: 100%;
  }

  h4 {
    margin-top: 15px;
    font-size: 20px;
    font-weight: 700;
    text-align: left;
    letter-spacing: 0;

    @media screen and (max-width: 732px) {
      text-align: center;
    }

    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }

  p {
    font-size: 12px;
    font-weight: 500;
    font-stretch: normal;
    line-height: 1.5;
    text-align: left;

    @media screen and (max-width: 732px) {
      font-size: 8px;
      line-height: 1.5;
      text-align: center;
    }

    a {
      border-bottom: 1px solid #19232d;
    }
  }
`;

export const CustomSwitchWrapper = styled.div<CustomSwitchWrapperProps>`
  text-align: left;
  flex-direction: 'column';

  @media screen and (max-width: 732px) {
    display: flex;
    margin-top: ${({ isChecked }) => (isChecked ? '0' : '20px')};
    justify-content: ${({ isChecked }) => (isChecked ? 'space-between' : 'center')};
  }
`;

export const CustomSwitchBlock = styled.div<CustomSwitchWrapperProps>`
  @media screen and (max-width: 732px) {
    text-align: ${({ isChecked }) => (isChecked ? 'left' : 'center')};
  }
`;

export const SwitchTitle = styled.p`
  font-size: 18px;
  margin: 5px 0 10px 0;
  font-weight: 700;

  @media screen and (max-width: 732px) {
    font-size: 12px;
    margin: 10px 0 10px 0;
    line-height: 1.15;
  }
`;

export const CustomSwitch = styled.label`
  position: relative;
  display: inline-block;
  width: 37px;
  height: 10px;

  input {
    opacity: 0;
    width: 0;
    height: 0;

    &:checked {
      + span {
        background-color: #c4d6e6;

        &:before {
          -webkit-transform: translateX(23px);
          -ms-transform: translateX(23px);
          transform: translateX(23px);
        }
      }
    }

    &:focus {
      + span {
        box-shadow: 0 0 1px #c4d6e6;
      }
    }
  }

  span {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: 0.4s;
    transition: 0.4s;
    border-radius: 34px;

    &:before {
      position: absolute;
      content: '';
      height: 14.4px;
      width: 14.4px;
      left: 0;
      bottom: -2.5px;
      background-color: white;
      -webkit-transition: 0.4s;
      transition: 0.4s;
      border: 1px solid #c4d6e6;
      border-radius: 50%;
    }
  }
`;

export const PpLink = styled.p`
  a {
    border-bottom: 1px solid #19232d;
  }
  @media screen and (max-width: 732px) {
    font-size: 10px;
  }
`;
