import styled from 'styled-components';

export const SubmitButtonContainer = styled.div`
`;

export const SubmitButton = styled.button.attrs({
  type: 'submit',
})`
  background-color: #19232d;
  color: white;
  border: 1px solid #19232d;
  font-family: Quicksand;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 3px;
  padding: 20px;
  width: fit-content;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  border-radius: 0px;
  margin-top: 20px;
`;
