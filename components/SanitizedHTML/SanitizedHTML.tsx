import React, { HTMLAttributes } from 'react';
import DOMPurify from 'isomorphic-dompurify';

type SanitizedHTMLProps = {
  innerHTML: string;
  as?: string;
}

export const sanitizeHtml = (html: string) => DOMPurify.sanitize(html);

export const SanitizedHTML: React.FC<SanitizedHTMLProps & HTMLAttributes<HTMLDivElement>> = (props: SanitizedHTMLProps) => {
  const { innerHTML, as, ...rest } = props;

  const cleanHTML = sanitizeHtml(innerHTML);

  return React.createElement(as || 'div', {
    ...rest,
    dangerouslySetInnerHTML: { __html: cleanHTML },
  });
};
