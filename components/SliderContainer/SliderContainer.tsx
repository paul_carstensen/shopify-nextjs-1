import React from 'react';
import styled, { css } from 'styled-components';

const SliderContainerInner = styled.div<{ backgroundColor?: string, backgroundImage?: StaticImageData, watermarkBg?: boolean, patternBg?: boolean }>`
  min-height: calc(100vh - 178px);
  scroll-snap-align: start;

  ${({ backgroundColor }) => `background-color: ${backgroundColor}`};

  ${({ backgroundImage, watermarkBg, patternBg }) => {
    if (backgroundImage && watermarkBg) {
      return css`
        background-image: url(${backgroundImage.src});
        background-position-x: right;
        background-repeat: no-repeat;
        background-size: 60%;
      `;
    }

    if (backgroundImage && patternBg) {
      return css`
        background-image: url(${backgroundImage.src}), url(${backgroundImage.src});
        background-repeat: no-repeat;
        background-position-x: center;
        background-position-y: 10%, 90%;
        background-size: 70%;
      `;
    }

    return null;
  }};

  & h1 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
    font-size: 2em;
    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }

  & h2 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
    font-size: 1.72em;
    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }

  & div.slider-container {
    min-height: calc(100vh - 178px);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 4% 10%;
    
    & h1, h2, h3 {
      line-height: normal
    };

    & .slider-container-main h1.title {
      font-size: 46px;
      text-transform: none;
      font-weight: bold;
      padding-bottom: 35px;
    }
  
    & .slider-container-main h2.subtitle {
      font-size: 22px;
      text-transform: none;
      font-weight: 600;
    }
  
    & .slider-container-main p.text {
      font-size: 18px;
    }

    & .slider-container-head  {
      text-align: right;
    }

    & .slider-container-head span {
      font-size:46px;
      font-weight: bold;
    }
  }

  @media screen and (max-width: 768px) {
    width: 100%;
    height: 100%;
    min-height: calc(100vh - 178px);

    ${({ backgroundImage, watermarkBg, patternBg }) => {
    if (backgroundImage && watermarkBg) {
      return css`
        background-size: 90%;
        background-position-x: 40vw;
      `;
    }

    if (backgroundImage && patternBg) {
      return css`
        background-position-x: center, center;
        background-position-y: 3%, 97%;
        background-size: 85%;
      `;
    }

    return null;
  }};

    & div.slider-container {
      min-height: calc(100vh - 178px);
      padding: 25px;
      max-width: inherit;

      & .slider-container-main h1.title {
        font-size: 26px;
      }

      & .slider-container-main h2.subtitle {
        font-size: 16px;
      }

      & .slider-container-main p.text {
        font-size: 14px;
      }

      & .slider-container-head span {
        font-size: 26px;

        display: block;
        min-height: 90px;
      }
    }
  }
`;

export interface SliderContainerProps {
  counter?: string;
  numSlides?: string;
  title?: string;
  subtitle?: string;
  text?: string;
  backgroundColor?: string;
  backgroundImage?: StaticImageData;
  watermarkBg?: boolean;
  patternBg?: boolean;
  children?: React.ReactNode;
  id?: string;
}

export const SliderContainer: React.FC<SliderContainerProps> = ({
  children,
  title,
  subtitle,
  text,
  backgroundColor,
  backgroundImage,
  watermarkBg,
  patternBg,
  counter,
  numSlides,
}: SliderContainerProps) => (
  <SliderContainerInner
    id={counter ? `tipp-slide-${counter}` : undefined}
    backgroundColor={backgroundColor}
    backgroundImage={backgroundImage}
    watermarkBg={watermarkBg}
    patternBg={patternBg}
  >
    <div className="slider-container">
      <div className="slider-container-head">
        {counter && numSlides ? <span className="count">{counter}/{numSlides}</span> : false}
      </div>

      <div className="slider-container-main">
        {title && <h1 className="title">{title}</h1>}
        {subtitle && <h2 className="subtitle">{subtitle}</h2>}
        {text && <p className="text">{text}</p>}

        {children}
      </div>

      <div className="slider-container-footer" />
    </div>
  </SliderContainerInner>
);
