import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import newsletterImage from '../../assets/images/newsletter/10-euro-newsletter.png';
import { CtaButton } from '../common/Buttons';

interface NewsletterContainerProps {
  imagePosition: 'left' | 'right';
}

interface SubTitleProps {
  paddingLeft?: string;
}

interface ContentProps {
  flex: string;
}

const NewsletterContainer = styled.div<NewsletterContainerProps>`
  display: flex;
  align-items: left;
  margin: 50px 0 0 0;
  font-family: Quicksand;
  letter-spacing: normal;
  flex-direction: ${(props) => (props.imagePosition === 'right' ? 'row' : 'row-reverse')};
  @media screen and (max-width: 768px) {
    margin: 30px -20px 0 -20px;
    flex-direction: ${(props) => (props.imagePosition === 'right' ? 'column' : 'column-reverse')};
  }
`;

const Content = styled.div<ContentProps>`
  padding: 0px 40px;
  flex: ${(props) => (props.flex)};
  @media screen and (max-width: 768px) {
    display: flex;
    padding: 0px 20px;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
  }
`;

const Title = styled.div`
  font-size: 32px;
  line-height: 1.5;
  font-weight: 500;
  padding-bottom: 30px;
  @media screen and (max-width: 768px) {
    font-size: 24px;
    line-height: 1.33;
  }
`;

const SubTitle = styled.div<SubTitleProps>`
  font-size: 22px;
  line-height: 1.09;
  font-weight: 500;
  padding-bottom: 20px;
  @media screen and (max-width: 768px) {
    font-size: 18px;
    line-height: 1.33;
    margin-top: 27px;
  }
`;

const SmallText = styled.div`
  font-size: 12px;
  font-weight: 500;
  line-height: 1.5;
`;

export interface NewsletterProps {
  title: string;
  subtitle: string;
  smallText?: string;
  imagePosition?: 'left' | 'right';
  ctaText?: string;
  ctaUrl?: string;
  ctaNewWindow?: boolean;
  trackingId?: string;
}

export const Newsletter: React.FC<NewsletterProps> = (props: NewsletterProps) => {
  const {
    title, subtitle, smallText, imagePosition, ctaText, ctaUrl, ctaNewWindow, trackingId,
  } = props;

  const renderBtn = () => (
    <CtaButton
      role="button"
      href={ctaUrl}
      target={ctaNewWindow ? '_blank' : undefined}
      rel={ctaNewWindow ? 'noopener noreferrer' : undefined}
      data-tracking-id={trackingId}
    >
      {ctaText}
    </CtaButton>
  );

  return (
    <NewsletterContainer imagePosition={imagePosition || 'right'}>
      <Content flex="0.6">
        <SubTitle>{subtitle}</SubTitle>
        <Title>{title}</Title>
        {ctaText && ctaUrl && renderBtn()}
        {smallText && (
        <SmallText>{smallText}</SmallText>
        )}
      </Content>
      <Content flex="0.4">
        <Image
          src={newsletterImage}
          alt="10€ Rabatt auf Deine nächste Bestellung"
          width="464"
          height="330"
        />
      </Content>
    </NewsletterContainer>
  );
};
