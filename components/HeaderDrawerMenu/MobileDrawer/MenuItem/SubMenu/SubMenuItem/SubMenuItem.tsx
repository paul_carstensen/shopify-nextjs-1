import React from 'react';
import { NavigationItem } from '../../../../../../types';
import { NavToggleButton } from '../../NavToggleButton';

type SubMenuItemProps = {
  link: NavigationItem;
  controlId: string;
}

export const SubMenuItem: React.FC<SubMenuItemProps> = ({ link, controlId }) => {
  if (link.items.length === 0) {
    return (
      <li className="mobile-nav__item">
        <a href={link.url} className="mobile-nav__link" id={controlId}>{link.title}</a>
      </li>
    );
  }

  return (
    <li className="mobile-nav__item">
      <NavToggleButton className="mobile-nav__child-item" buttonClassName="mobile-nav__link--button collapsible-trigger" buttonControlId={controlId}>
        <span className="mobile-nav__faux-link">{link.title}</span>
      </NavToggleButton>

      <div
        id={controlId}
        className="mobile-nav__sublist collapsible-content collapsible-content--all "
      >
        <div className="collapsible-content__inner">
          <ul className="mobile-nav__grandchildlist">
            {
              link.items.map((subLink) => (
                <li className="mobile-nav__item" key={`${controlId}-${subLink.title}`}>
                  <a href={subLink.url} className="mobile-nav__link">{subLink.title}</a>
                </li>
              ))
            }
          </ul>
        </div>
      </div>
    </li>
  );
};
