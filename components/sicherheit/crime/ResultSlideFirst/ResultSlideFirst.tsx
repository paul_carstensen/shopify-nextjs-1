import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { SliderContainer } from '../SliderContainer';
import arrowDown from '../../../../assets/images/crime-check/arrow-down-black.svg';

export const RIGHT_SLIDE_FIRST_CONTAINER_ID = 'crime-check-right-slide-first-container';

export interface ResultSlideFirstProps {
  counter?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
  onArrowButtonClick: () => void;
}

type ArrowButtonProps = {
  height: number;
};

const ArrowButton = styled.div<ArrowButtonProps>`
  display: block;
  text-align: center;
  height: ${(props) => props.height}px;
  cursor: pointer;

  @media screen and (max-width: 768px) {
    display: block;
    text-align: center;
    height: ${(props) => props.height}px;
  }
`;

const ResultSlideFirstInner = styled.div`
  height: calc(100vh - 178px);
  width: 100%;
  display: grid;
  grid-template-rows: 1fr auto 1fr;
  grid-template-areas: "top" "contents";

  @media screen and (max-width: 768px) {
    height: calc(100vh - 120px);
    width: 100%;

    #tipp-slide-1 {
      height: calc(100vh - 120px);
    }
  }
`;

export const ResultSlideFirst: React.FC<ResultSlideFirstProps> = ({ backgroundColor, counter, children, onArrowButtonClick }) => (
  <ResultSlideFirstInner id={RIGHT_SLIDE_FIRST_CONTAINER_ID}>
    <SliderContainer
      counter={counter}
      backgroundColor={backgroundColor}
    >
      {children}
      <ArrowButton height={arrowDown.height} onClick={onArrowButtonClick}>
        <Image src={arrowDown} alt="Weiter" />
      </ArrowButton>
    </SliderContainer>
  </ResultSlideFirstInner>
);
