import * as Yup from 'yup';

export const postalCodeSchema = Yup.object().shape({
  postalCode: Yup.string().required('Postleitzahl wird benötigt').matches(/^\d{5}$/, 'Ungültige Postleitzahl'),
});
