import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { SliderContainer } from '../SliderContainer';
import { ContentDescription } from '../ContentDescription';
import { ResultTable } from '../Table';
import arrowDown from '../../../../assets/images/crime-check/arrow-down.svg';
import { CrimeInfo } from '../../../../types/sicherheit/wie-sicher-ist-dein-wohnort';

export const RIGHT_SLIDE_SECOND_CONTAINER_ID = 'crime-check-right-slide-second-container';

export interface ResultSlideSecondProps {
  counter?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
  crimeCheckInfo: CrimeInfo;
  onArrowButtonClick: () => void;
  buttonUrl: string;
  buttonTrackingId?: string;
}

type ArrowButtonProps = {
  height: number;
};

const ArrowButton = styled.div<ArrowButtonProps>`
  display: block;
  text-align: center;
  height: ${(props) => props.height}px;
  margin-top: 15px;
  cursor: pointer;

  @media screen and (max-width: 768px) {
    display: block;
    text-align: center;
    height: ${(props) => props.height}px;
  }
`;

const ResultContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 10px;
`;

const Result = styled.output`
  background-color: #c4d6e6;
  color: #19232d;
  border: 1px solid #c4d6e6;
  border-radius: 50px;
  font-size: 13px;
  font-weight: 600;
  line-height: normal;
  padding: 8px 10px;
  text-align: center;
  margin-right: 10px;
`;

const TableWrapper = styled.div`
  margin-left: -25px;
  margin-right: -25px;
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;

const Button = styled.button`
  background-color: #c4d6e6;
  color: #19232d;
  border: 1px solid #c4d6e6;
  font-size: 12px;
  font-weight: 500;
  line-height: normal;
  letter-spacing: 3px;
  padding: 10px;
  text-transform: uppercase;
  text-align: center;
  margin-top: 33px;
  @media screen and (max-width: 768px) {
    margin-top: 20px;
  }
`;

export const ResultSlideSecond: React.FC<ResultSlideSecondProps> = ({ backgroundColor, counter, children, crimeCheckInfo, onArrowButtonClick, buttonUrl, buttonTrackingId }) => (
  <div id={RIGHT_SLIDE_SECOND_CONTAINER_ID}>
    <SliderContainer
      counter={counter}
      backgroundColor={backgroundColor}
      counterFontColor="white"
    >
      {children}
      <ResultContainer>
        <Result>
          {crimeCheckInfo.cityRelativeRisk.locationName} - {crimeCheckInfo.cityRelativeRisk.relativeRisk}
        </Result>
        {
          crimeCheckInfo.districtRelativeRisk
          && (
            <Result>
              {crimeCheckInfo.districtRelativeRisk.locationName} - {crimeCheckInfo.districtRelativeRisk.relativeRisk}
            </Result>
          )
        }
      </ResultContainer>
      <ResultContainer>
        <Result>
          {crimeCheckInfo.caseAmountSummary.numCases.toLocaleString('de-DE')}
        </Result>
        <ContentDescription fontColor="white">
          Anzahl begangener Straftaten: {crimeCheckInfo.caseAmountSummary.locationName} {crimeCheckInfo.caseAmountSummary.timeFrame}
        </ContentDescription>
      </ResultContainer>
      <TableWrapper>
        <ResultTable resultData={crimeCheckInfo.crimeStatsPerType} />
      </TableWrapper>
      <a href={buttonUrl} target="_blank" rel="noreferrer">
        <ButtonContainer>
          <Button data-tracking-id={buttonTrackingId}>
            Weitere Informationen
          </Button>
        </ButtonContainer>
      </a>
      <ArrowButton height={arrowDown.height} onClick={onArrowButtonClick}>
        <Image src={arrowDown} alt="Weiter" />
      </ArrowButton>
    </SliderContainer>
  </div>
);
