import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { FontFamilyPoppins } from '@u2dv/marketplace_ui_kit/dist/styles';

export type Option = {
  title: string;
  dataTrackingId: string;
  url?: string;
  onClick?: () => void;
  lifeStyle?: string[];
  dataTestId?: string;
};

type QuestionCTAProps = {
  title: string;
  bgImage?: string;
  maxWidth?: string;
  minHeight?: string;
  options: Option[];
  nextSlide?: () => void;
  pagination?: number;
  totalPagination?: number;
  center?: boolean;
  btnFontSizeLarge?: number;
  minHeightDesktop?: string;
}

type QuestionCTAContainerProps = {
  bgImage?: string;
  maxWidth?: string;
  minHeight?: string;
  center?: boolean;
  minHeightDesktop?: string;
}

type QuestionCTAButtonProps = {
  btnFontSizeLarge?: number;
}

const QuestionCTAContainer = styled.div<QuestionCTAContainerProps>`
  margin: auto;
  ${(props) => props.maxWidth && `max-width: ${props.maxWidth};`}
  ${(props) => props.minHeight && `min-height: ${props.minHeight};`}
  padding: 0 30px 0 30px;
  text-align: center;
  ${(props) => props.bgImage && `background-image: url("${props.bgImage}");`}
  background-size: cover;
  background-repeat: no-repeat;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  display: flex;
  flex-direction: column;
  justify-content: ${(props) => (props.center ? 'center' : 'space-between')};

  @media screen and (min-width: 768px) {
    ${(props) => props.minHeightDesktop && `min-height: ${props.minHeightDesktop};`}
  }
`;

const Title = styled.p`
  color: white;
  font-family: ${FontFamilyPoppins};
  font-weight: bold;
  font-size: 20px;
  line-height: 1.44;
  hyphens: auto;

  margin-bottom: 40px;
`;

const QuestionOptionContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const QuestionOptionLink = styled.a`
  padding: 3px 9px;
  background-color: #f7f7f7;
  border-radius: 20px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1);
  font-size: 12px;
  margin-bottom: 16px;
`;

const QuestionOptionButton = styled.button<QuestionCTAButtonProps>`
  padding: 7.5px 10px;
  background-color: #f7f7f7;
  border-radius: 20px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1);
  font-size: 12px;
  ${(props) => props.btnFontSizeLarge && `font-size: ${props.btnFontSizeLarge}px;`}
  font-weight: bold;
  margin-bottom: 16px;
  min-width: 160px;
`;

const Pagination = styled.div`
  color: white;
  font-size: 20px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 10px;
`;

const SurveyContainer = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const renderOption = (
  btnFontSizeLarge: number | undefined,
  { title, dataTrackingId, url, onClick, dataTestId }: Option,
  nextSlide?: () => void,
) => {
  if (url) {
    return (
      <Link passHref key={`option-${title}`} href={url}>
        <QuestionOptionLink data-tracking-id={dataTrackingId} data-testid={dataTestId}>
          {title}
        </QuestionOptionLink>
      </Link>
    );
  }
  return (
    <QuestionOptionButton
      key={`option-${title}`}
      data-tracking-id={dataTrackingId}
      onClick={() => {
        onClick?.();
        nextSlide?.();
      }}
      btnFontSizeLarge={btnFontSizeLarge}
      data-testid={dataTestId}
    >
      {title}
    </QuestionOptionButton>

  );
};

const renderOptions = (btnFontSizeLarge: number | undefined, options: Option[], nextSlide?: () => void) => (
  options.map((option) => renderOption(btnFontSizeLarge, option, nextSlide))
);

export const QuestionCTA: React.FC<QuestionCTAProps> = ({
  title,
  bgImage,
  maxWidth,
  minHeight,
  options,
  nextSlide,
  pagination,
  totalPagination,
  center,
  btnFontSizeLarge,
  minHeightDesktop,
}) => (
  <QuestionCTAContainer
    key={`question-${title}`}
    bgImage={bgImage}
    maxWidth={maxWidth}
    minHeight={minHeight}
    center={center}
    minHeightDesktop={minHeightDesktop}
  >
    <SurveyContainer>
      <Title>{title}</Title>
      <QuestionOptionContainer>
        {renderOptions(btnFontSizeLarge, options, nextSlide)}
      </QuestionOptionContainer>
    </SurveyContainer>
    {
        pagination ? (
          <Pagination>
            {pagination}/{totalPagination}
          </Pagination>
        ) : (
          null)
    }
  </QuestionCTAContainer>
);
