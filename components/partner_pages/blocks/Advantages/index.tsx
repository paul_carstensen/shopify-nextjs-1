import Image from 'next/image';
import { BlockContent as Props } from 'types/partnerPages';
import isInsideLinkAnchor from '../../../../helpers/partner_pages/isInsideLinkAnchor';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import emptyBigImage from '../../../../assets/images/partner-pages/empty-big.png';

interface Content {
  icon: string;
  title: string;
  description: string
}

export interface BlockProps {
  title: string;
  buttonText: string;
  buttonUrl: string;
  content: Content[];
  identifier: string;
}

const Block = ({ data }: Props) => {
  const {
    title, buttonText, buttonUrl, content, identifier,
  } = data as BlockProps;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={`${commonStyles.container} ${styles.advantages}`} id={identifier}>
        <div className={commonStyles['page-width']}>
          <div className={commonStyles['inner-container']}>
            <div>
              <h2 className={commonStyles.dark}>{title}</h2>
              <div>
                {isInsideLinkAnchor(buttonUrl)
                  ? <a href={buttonUrl} className={`${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-advantage-cta']}`} role="button">{buttonText}</a>
                  : <a href={buttonUrl} target="_blank" rel="noopener noreferrer" className={`${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-advantage-cta']}`} role="button">{buttonText}</a>}
              </div>
            </div>
            <div className={styles['content-container']}>
              {content?.map((t, i) => (
                <div key={`content-${i.toString()}`} className={styles['advantage-container']}>
                  <Image
                    src={t.icon.length > 0 ? t.icon : emptyBigImage.src}
                    width={160}
                    height={160}
                    className={styles['advantage-image']}
                    alt={t.description}
                  />
                  <div className={styles['text-blocks']}>
                    <div className={styles['advantage-title-wrapper']}>
                      <h3 className={styles['advantage-title']}>{t.title}</h3>
                    </div>
                    <p className={styles['advantage-description']}>
                      {t.description}
                    </p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
