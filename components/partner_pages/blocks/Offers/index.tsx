import React, { useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';
import Slider from 'react-slick';
import { BlockContent as Props } from 'types/partnerPages';
import isInsideLinkAnchor from '../../../../helpers/partner_pages/isInsideLinkAnchor';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import emptyBigImage from '../../../../assets/images/partner-pages/empty-big.png';

interface Content {
  title: string;
  description: string;
  buttonText: string;
  buttonUrl: string;
  image: string;
}

export interface BlockProps {
  title: string;
  content: Content[];
  identifier: string;
}

const slickSettings = {
  className: 'offers-slider',
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 0,
  adaptiveHeight: true,
  swipeToSlide: true,
  arrows: true,
};

const Block: React.FC<Props> = ({ data }: Props) => {
  const { title, content, identifier } = data as BlockProps;

  // Slick Slider has an issue where it does not render correctly in SSR scenarios.
  // This is a preliminary workaround to make it render only on the client side.
  const [shouldRenderSlider, setShouldRenderSlider] = useState<boolean>(false);

  useEffect(() => {
    setShouldRenderSlider(true);
  }, [shouldRenderSlider]);

  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
  const slideWrapper = (children: React.ReactNode): React.ReactNode => (isMobile && shouldRenderSlider ? <Slider {...slickSettings}>{children}</Slider> : children);
  const sliderParentClasses = isMobile ? `${styles['content-container']} ${styles['offers-flex']}` : `${styles['content-container']} ${styles['offers-flex']} ${styles['overflow-content']}`;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={`${commonStyles.container} ${styles.offers}`} id={identifier}>
        <div className={commonStyles['page-width']}>
          <div className={commonStyles['inner-container']}>
            <div>
              <h2 className={commonStyles.dark}>{title}</h2>
            </div>
            <div className={isMobile ? '' : styles['overflow-wrapper']}>
              <div className={sliderParentClasses}>
                {slideWrapper(content?.map((t, i) => (
                  <div key={`content-${i.toString()}`} className={styles['offers-flex-element']}>
                    <div
                      className={styles.image}
                      style={{
                        backgroundImage: `url(${t.image.length > 0 ? t.image : emptyBigImage.src})`,
                        backgroundPosition: isMobile ? 'left' : 'center',
                        backgroundSize: 'contain',
                        backgroundRepeat: 'no-repeat',
                      }}
                    />
                    <h3 className={styles['offer-title']}>{t.title}</h3>
                    <div className={styles['offer-description']}>
                      {t.description}
                    </div>
                    <div>
                      {isInsideLinkAnchor(t.buttonUrl)
                        ? <a href={t.buttonUrl} className={`${commonStyles.btn} ${commonStyles['btn--secondary']} ${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-offer']}`} role="button">{t.buttonText}</a>
                        : <a href={t.buttonUrl} target="_blank" rel="noopener noreferrer" className={`${commonStyles.btn} ${commonStyles['btn--secondary']} ${commonStyles.button} ${commonStyles.light} ${styles['ga-partner-page-offer']}`} role="button">{t.buttonText}</a>}
                    </div>
                  </div>
                )))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
