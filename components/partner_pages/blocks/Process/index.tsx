import React from 'react';
import { useMediaQuery } from 'react-responsive';
import { BlockContent as Props } from 'types/partnerPages';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import emptyBigImage from '../../../../assets/images/partner-pages/empty-big.png';

interface Content {
  icon: string;
  description: string
}

export interface BlockProps {
  title: string;
  description: string;
  content: Content[];
  identifier: string;
}

const Block: React.FC<Props> = ({ data }: Props) => {
  const {
    title, description, content, identifier,
  } = data as BlockProps;

  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={`${commonStyles.container} ${styles.process}`} id={identifier}>
        <div>
          <div className={isMobile ? undefined : commonStyles['inner-container']}>
            <div>
              <h2 className={commonStyles.dark}>{title}</h2>
              <p className={styles.description}>{description}</p>
            </div>
            <div className={styles['content-container']}>
              {content.map((t, i) => (
                <div key={`content-${i.toString()}`} className={styles.item}>
                  <img
                    src={t.icon.length > 0 ? t.icon : emptyBigImage.src}
                    className={styles.image}
                    alt={t.description}
                  />
                  <p className={styles['process-description']}>
                    {t.description}
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
