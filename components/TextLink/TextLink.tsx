import styled from 'styled-components';

export const TextLink = styled.a`
  display: inline-block;
  position: relative;
  text-decoration: none;
  border-bottom: 1px solid rgba(25,35,45,.15);
  color: #19232d;

  &:after {
    content: "";
    position: absolute;
    bottom: -2px;
    left: 0;
    border-bottom: 2px solid currentColor;
    transition: width .5s ease;
  }

  &:hover:after {
    width: 100%;
  }

  &:not(:hover):after {
    width: 0%;
  }
`;
