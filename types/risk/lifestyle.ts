export type QuestionRisk = {
  question: string;
  answerId: number;
  answer: string[];
};

export type KeepMeUpdatedBody = {
  email: string | null;
  submittedQuestions: QuestionRisk[];
  selectedLifestyleKey: string;
};

export type Lifestyle = {
  lifestyleKey:
    'autopilot' |
    'freedom_mind' |
    'gatekeeper' |
    'hip_and_hopper' |
    'le_hipster' |
    'luxury_lover' |
    'mind_maker' |
    'multitasker' |
    'online_avatar' |
    'silver_athlete' |
    'social_politician' |
    'solo_dj' |
    'storyteller' |
    'sun_herdsman' |
    'urban_guy' |
    'visionary' |
    'world_pilot' |
    'yogi_guru';
  title: string;
  kpiAllPercent: number;
  kpiAllAmount: number;
  kpiAllPrediction: string;
  kpiCoregroupAmount: number;
  kpiCoregroupAge: string;
  kpiIncome: number;
  statementBeliefs: string;
  statementMotto: string;
  statementLifegoal: string;
  shortDescription: string;
  description: string;
  score?: number;
}
