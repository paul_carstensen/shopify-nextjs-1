export type AddressData = {
  email: string;
  houseNumber: number | string;
  postalCode: number;
  streetName: string;
}

export type ArabeskData = {
  arabeskWaterFilter: boolean;
  arabeskWaterFilterMaintenance: boolean;
  arabeskWaterRarelyUsedSources: boolean;
  arabeskWaterSiliconeGroutOk: boolean;
  arabeskSmokeAlarms: boolean;
  arabeskCircuitBreakers: boolean;
  arabeskPropertyOwner: boolean;
  arabeskMovingOutSoon: boolean;
}
