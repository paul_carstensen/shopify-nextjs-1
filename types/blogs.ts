export type BlogPost = {
    blogPostId: string;
    title: string;
    seoTitle?: string;
    slug: string;
    description?: string;
    imageUrl?: string;
    published: boolean;
    publishedAt?: string;
    readingTimeMinutes?: number;
    tagsString?: string;
    authorName: string;
    contents: string;
};

export type Blog = {
    blogId: string;
    name: string;
    slug: string;
    description?: string
    imageUrl?: string;
    posts: BlogPost[];
}
