// @ts-check

// eslint-disable-next-line @typescript-eslint/no-var-requires
const config = require('./config');

/**
 * @type {import('next').NextConfig}
 * */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['partner-images.uptodate.de', 'cdn.shopify.com'],
  },
  typescript: {
    ignoreBuildErrors: true, // skip type checking because we call tsc inside the npm build script and check all files
  },
  async headers() {
    return [
      // Send far-future expiry headers for images with hashed filenames (32 chars).
      {
        source: '/_next/static/image/(.*).([a-f0-9]{32}).(jpg|png|webp|avif|svg|gif)',
        locale: false,
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=315360000, immutable',
          },
        ],
      },
      // Send the x-shopid response header used by Shopify to verify domains
      {
        source: '/:path*',
        locale: false,
        headers: [
          {
            key: 'X-ShopId',
            value: config().shop.shopId,
          },
        ],
      },
    ];
  },
};

module.exports = nextConfig;
