/** @jest-environment jsdom */
import { getPage } from 'next-page-tester';
import { mocked } from 'ts-jest/utils';
import { screen, fireEvent } from '@testing-library/react';
import { getNavigationMenus } from '../../../helpers/getNavigationMenus';
import { getPolicyText } from '../../../helpers/consent_tracker/api';

jest.mock('next/image', () => ({
  __esModule: true,
  default: () => <></>, // eslint-disable-line react/display-name
}));

jest.mock('../../../helpers/getNavigationMenus');
jest.mock('../../../helpers/consent_tracker/api');

const renderPage = async () => {
  const page = await getPage({
    route: '/nachhaltigkeit/radon',
  });

  return page.render();
};

describe('Radon page', () => {
  beforeAll(() => {
    mocked(getNavigationMenus).mockResolvedValue({
      contentMain: [],
      contentSide: [],
      contentFooter: [],
    });
  });

  it('should render the expected contents', async () => {
    mocked(getPolicyText).mockResolvedValueOnce('Policy text');

    await renderPage();

    expect(getPolicyText).toHaveBeenCalledTimes(1);

    // Hero
    expect(screen.getByTestId('hero')).toBeInTheDocument();

    // Sharing bar
    expect(screen.getByText('Jetzt teilen')).toBeInTheDocument();

    // Split View
    expect(screen.getByText('Warum ist Radon potenziell gefährlich?')).toBeInTheDocument();
  });

  it('should focus the form when the CTA button is clicked', async () => {
    await renderPage();

    const ctaButton = screen.getByText('Radon-Analyse starten');
    const emailField = screen.getByTestId('radon-address-form-first-field');

    fireEvent.click(ctaButton);

    expect(document.activeElement).toEqual(emailField);
  });
});
