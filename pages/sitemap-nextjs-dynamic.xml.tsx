import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import { getServerSideSitemap, ISitemapField } from 'next-sitemap';
import { fetchMarketplaceGraphQL } from '../helpers/fetchMarketplaceGraphQL';
import { BlogPost } from '../types/blogs';
import config from '../config';

const DynamicSitemapPage: NextPage = () => <></>;

const toSitemapField = (path: string, lastModified: Date | undefined = new Date()): ISitemapField => ({
  loc: new URL(path, process.env.SITE_URL).href,
  lastmod: lastModified.toISOString(),
});

// TODO: fetch partner page handles and modification dates from database
const getPartnerPageSitemapFields = async (): Promise<ISitemapField[]> => {
  const paths = [
    '/partners/8fit',
    '/partners/runtastic',
  ];

  return paths.map((path) => toSitemapField(path));
};

const GET_BLOG_POSTS_QUERY = `
  query getBlogPosts($blogSlug: String!) {
    blogPosts(blogSlug: $blogSlug) {
      slug
    }
  }
`;

const fetchBlogPosts = async (): Promise<Pick<BlogPost, 'slug'>[]> => {
  const { data, errors } = await fetchMarketplaceGraphQL(config(), GET_BLOG_POSTS_QUERY, {
    variables: {
      blogSlug: config().blog.defaultBlogSlug,
    },
  });

  if (errors?.length) {
    throw new Error(errors[0].message);
  }

  return data.blogPosts;
};

const getBlogSitemapFields = async (): Promise<ISitemapField[]> => {
  const posts = await fetchBlogPosts();

  return posts.map(({ slug }) => (
    // Ideally this should use a "last updated" field of the blog post
    toSitemapField(`/blog/${slug}`)));
};

export const getServerSideProps: GetServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const fields: ISitemapField[] = [
    ...await getPartnerPageSitemapFields(),
    ...await getBlogSitemapFields(),
  ];

  return getServerSideSitemap(ctx, fields);
};

export default DynamicSitemapPage;
