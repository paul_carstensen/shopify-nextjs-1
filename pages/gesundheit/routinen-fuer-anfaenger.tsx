import React from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { colors } from '@u2dv/marketplace_ui_kit/dist/styles';
import { Button, Slider, SlideProps } from '@u2dv/marketplace_ui_kit/dist/components';
import { scrollToSlider } from '../../helpers/scrollToSlider';
import { SliderPage, SLIDER_PAGE_SCROLL_CONTAINER_ID } from '../../components/SliderPage';
import { SliderContainer } from '../../components/SliderContainer';
import { SlideIntro } from '../../components/SlideIntro';
import { ButtonBar } from '../../components/ButtonBar';
import coffeeCupsImage from '../../assets/images/routines/coffee-cups.svg';
import flowerPotsImage from '../../assets/images/routines/flower-pots.svg';
import bedsImage from '../../assets/images/routines/beds.svg';

const SliderContentWrapper = styled.div`
  margin-left: -32px;
  margin-right: -32px;

  & h3 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    letter-spacing: 0em;
    line-height: 1;
    font-size: 1.4em;
    text-transform: none;
    font-weight: 600;

    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }
`;

type RoutineSliderProps = SlideProps & {
  onSkipClick: () => void;
}

const MorningRoutineSlider: React.FC<RoutineSliderProps> = ({ onSkipClick }) => (
  <SliderContainer
    counter="1"
    numSlides="3"
    title="Morgenroutine: Starte besser in den Tag"
    text="Wir zeigen Dir 5 einfache Dinge, um in nur 3 Minuten Deinen Tag besser zu beginnen."
    backgroundColor="#ede3bb"
    backgroundImage={coffeeCupsImage}
    patternBg
  >
    <SliderContentWrapper>
      <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
        <SlideIntro
          padding={32}
          text=""
          startButtonTrackingId="ga-routines-morning-continue"
          onSkipClick={onSkipClick}
          skipButtonText="Nein, eine andere Routine"
          skipButtonTrackingId="ga-routines-morning-skip"
        />

        <Slider visibleSlidesBreakpoints={1} enableArrows enableMouseWheel>
          <div>
            <h3>Routine 1: Raus aus den Federn</h3>
            <p>Stehe ein paar Minuten früher auf – sie werden Dir dabei helfen, weniger Zeitdruck zu haben. Verbanne am besten die Snooze-Taste und nutze die wertvolle Zeit lieber, um in den Tag zu starten. Ein Lichtwecker bereitet Deinen Körper auf das Aufwachen vor, während Du noch schläfst. Das Licht hilft bei der Ausschüttung von Cortisol – so fühlst Du Dich beim Aufwachen deutlich erholter und wirst nicht aus dem Schlaf gerissen.</p>
          </div>

          <div>
            <h3>Routine 2: Atme bewusst und tief</h3>
            <p>Direkt nach dem Aufwachen hat Dein Blut die höchste Konzentration an Cortisol und sorgt dafür, dass Du voller Energie in den Tag starten kannst. Tiefe Atemzüge oder Gähnen helfen, den gesamten Körper hochzufahren. Atme 1 Minute tief und bewusst in den Bauch ein und aus.</p>
          </div>

          <div>
            <h3>Routine 3: Reck & Streck Dich</h3>
            <p>Ausgiebiges Recken und Strecken wecken den Körper optimal auf – Deine Zellen werden mit frischem Sauerstoff befüllt und der Kreislauf wird in Schwung gebracht.</p>
          </div>

          <div>
            <h3>Routine 4: Bleib offline statt online</h3>
            <p>Was Du morgens als erstes siehst, beeinflusst wie Du in den Tag startest. Lass am besten Smartphone, Radio und Co. eine Runde länger schlafen und nutze einen klassischen Wecker. Schlechte Nachrichten, Arbeitsemails & Social Media setzen Dich sonst schon unter Druck, bevor Du überhaupt richtig wach bist.</p>
          </div>

          <div>
            <h3>Routine 5: Fülle Deinen Wasserhaushalt auf</h3>
            <p>Ein Glas Wasser am Morgen vertreibt Kummer und Sorgen. Und weil viel auch viel hilft, raten wir zu zwei Gläsern lauwarmem Wasser. So gleichst Du den Flüssigkeitsverlust der Nacht wieder aus und überforderst Deinen Magen nicht gleich.</p>

            <ButtonBar>
              <Button as="a" href="https://www.uptodate.de/collections/schlaf?utm_medium=routinebutton&utm_content=morgenroutine">Hilfreiche Produkte</Button>
              <Button appearance="secondary" onClick={onSkipClick} trackingId="ga-routines-morning-finish">Weiter</Button>
            </ButtonBar>
          </div>
        </Slider>
      </Slider>
    </SliderContentWrapper>
  </SliderContainer>
);

const EveningRoutineSlider: React.FC<RoutineSliderProps> = ({ onSkipClick }) => (
  <SliderContainer
    counter="2"
    numSlides="3"
    title="Abendroutine: Starte besser in den Schlaf"
    text="42% der Deutschen schlafen schlecht. Wenn Du eine dieser Personen bist, haben wir hier 5 einfache Dinge, um besser zu schlafen."
    backgroundColor={colors.powderBlue}
    backgroundImage={bedsImage}
    patternBg
  >
    <SliderContentWrapper>
      <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
        <SlideIntro
          padding={32}
          text=""
          startButtonTrackingId="ga-routines-evening-continue"
          onSkipClick={onSkipClick}
          skipButtonTrackingId="ga-routines-evening-skip"
        />

        <Slider visibleSlidesBreakpoints={1} enableArrows enableMouseWheel>
          <div>
            <h3>Routine 1: Schlafrhythmus einhalten, besser schlafen.</h3>
            <p>Zu einer guten Abendroutine gehört auch, dass Du Deine Schlafenszeiten einhältst: Du solltest jeden Abend zur selben Zeit schlafen gehen und jeden Morgen zur selben Zeit aufstehen. Auch wenn es verlockend ist, achte darauf auch am Wochenende nicht mehr als 30 Minuten davon abzuweichen.</p>
          </div>

          <div>
            <h3>Routine 2: Stimmt die Umgebung, stimmt der Schlaf.</h3>
            <p>Beziehe Dein Bett mindestens ein mal die Woche neu und sorge möglichst für absolute Dunkelheit.</p>
            <p>Die Zimmertemperatur sollte nachts zwischen 17 und 22 Grad sein. Lüfte immer gut, bevor Du schlafen gehst.</p>
          </div>

          <div>
            <h3>Routine 3: Leichte Kost am Abend</h3>
            <p>Verzichte auf schwere Kost, da Dein Körper sonst in der Nacht mit der Verdauung beschäftigt ist. Deine letzte Mahlzeit sollte beim Schlafengehen mindestens zwei Stunden zurück liegen.</p>
          </div>

          <div>
            <h3>Routine 4: Display-Verbot</h3>
            <p>Nimm weder Handy noch Laptop mit ins Schlafzimmer. Die Wellenlängen des Displaylichts von Handy und Laptop signalisieren Deinem Körper, dass es Tag ist.</p>
          </div>

          <div>
            <h3>Routine 5: Verzicht auf Koffein und Alkohol</h3>
            <p>Dass Koffein wach macht, ist bekannt. Achte darauf, Deinen letzten Kaffee spätestens am frühen Nachmittag zu Dir zu nehmen.</p>
            <p>Alkohol lässt uns zwar gut einschlafen, allerdings schlafen wir nicht gut durch. Auch die Erholung im Schlaf unter Alkoholeinfluss ist weniger effektiv.</p>

            <ButtonBar>
              <Button as="a" href="https://www.uptodate.de/collections/schlaf?utm_medium=routinebutton&utm_content=abendroutine">Hilfreiche Produkte</Button>
              <Button appearance="secondary" onClick={onSkipClick} trackingId="ga-routines-evening-finish">Fertig</Button>
            </ButtonBar>
          </div>
        </Slider>
      </Slider>
    </SliderContentWrapper>
  </SliderContainer>
);

export const ConclusionSlide: React.FC = () => (
  <SliderContainer
    counter="3"
    numSlides="3"
    title="Super, nun bist Du mit Tipps ausgestattet, um besser zu schlafen!"
    text="In unserem Shop findest Du Produkte, die Dich besser aufstehen und schlafen lassen."
    backgroundColor="#ede3bb"
    backgroundImage={flowerPotsImage}
    patternBg
  >
    <Button as="a" href="https://www.uptodate.de/collections/schlaf?utm_medium=routinebutton&utm_content=finish">Hilfreiche Produkte</Button>
  </SliderContainer>
);

const title = 'Fit am Tag durch eine Morgenroutine und Abendroutine';
const description = 'Fühl Dich fitter mit Deiner Morgenroutine & Abendroutine – in nur 5 Minuten abends besser einschlafen und morgens erholter aufwachen ✓ Jetzt starten.';

const RoutinesPage: NextPage = () => (
  <>
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />

      <meta key="og:title" property="og:title" content={title} />
      <meta key="og:description" property="og:description" content={description} />
      <meta key="og:image" property="og:image" content="" />
    </Head>

    <SliderPage
      backgroundColor="#e6e6e6"
      title="Routinen im Alltag – schnell und einfach"
      subtitle="Mit weniger als 10 Minuten am Tag besser leben!"
      text="Ob besser aufwachen oder gut einschlafen. Wir zeigen Dir einfache und schnelle Routinen, mit denen Du besser leben wirst. Eine Routine ist eine Abfolge an Handlungen, die Du in Deinen täglichen Ablauf einbaust."
    >
      <MorningRoutineSlider onSkipClick={() => scrollToSlider('#tipp-slide-2', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <EveningRoutineSlider onSkipClick={() => scrollToSlider('#tipp-slide-3', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <ConclusionSlide />
    </SliderPage>
  </>
);

export default RoutinesPage;
