import React, { useCallback, useEffect, useState } from 'react';
import { NextPage } from 'next';
import styled from 'styled-components';
import { Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import { QuestionCTA } from 'components/QuestionCTA';
import { ProductList } from 'components/ProductList';
import { Intro } from 'components/Intro';
import { renderTeaser } from 'components/Teaser';
import { loadUserPreference, UserPreference, isUserPreference } from 'components/ProductList/userPreferences';
import { intro, questions, teasers, CategoryKey, InterestKey } from '../helpers/homePageConfig';

declare global {
  interface Window {
    hj?: (...args: string[]) => void;
  }
}

// HotJar does not detect when the product list is rendered and so does not
// track user behaviour. We have to manually notify it as a workaround.
const updateHotJar = (url: string) => window?.hj?.('stateChange', url);

const IntroContainer = styled.div`
  margin-bottom: 30px;
`;

const SliderContainer = styled.div`
  max-width: 300px;
  margin: 0 auto;
`;

const Container = styled.div`
  margin: auto;
  max-width: 950px;
  margin-bottom: 40px;
`;

const QuestionContainer = styled.div`
  max-width: 400px;
  margin: auto;
`;

type UserPreferenceQuestionsProps = {
  userPreference: Partial<UserPreference>;
  setUserPreference: (up: Partial<UserPreference>) => void;
};

const GetUserPreferenceDialog = ({ userPreference, setUserPreference }: UserPreferenceQuestionsProps) => {
  const onAnswer = useCallback(
    (idx: number, key: string) => {
      if (idx === 0) {
        // first question answered
        setUserPreference({ categories: [key as CategoryKey] });
      } else {
        // second question answered
        setUserPreference({ ...userPreference, interest: key as InterestKey });
      }
    },
    [setUserPreference, userPreference],
  );

  return (
    <QuestionContainer>
      <IntroContainer>
        <Intro
          title={intro.title}
          description={intro.description}
          explanations={intro.explanations}
          dataTrackingId={intro.dataTrackingId}
        />
      </IntroContainer>
      <SliderContainer>
        <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false} disableHorizontalScroll>
          {questions.map((question, idx) => (
            <QuestionCTA
              key={`question-${question.title}`}
              title={question.title}
              options={question.options.map((option) => ({
                ...option,
                onClick: () => onAnswer(idx, option.key),
              }))}
              minHeight="370px"
              bgImage={question.bgImage}
              pagination={idx + 1}
              totalPagination={questions.length}
            />
          ))}
        </Slider>
      </SliderContainer>
    </QuestionContainer>
  );
};

const HomePage: NextPage = () => {
  const [userPreference, setUserPreference] = useState<Partial<UserPreference>>();
  const hasPreference = isUserPreference(userPreference);

  useEffect(() => {
    setUserPreference(loadUserPreference());
  }, [setUserPreference]);

  useEffect(() => {
    if (hasPreference) {
      updateHotJar('/?product-list');
    }
  }, [hasPreference]);

  return (
    <Container>
      {userPreference &&
        (hasPreference ? (
          <>
            {renderTeaser(teasers.slot1)}
            <ProductList userPreference={userPreference} />
          </>
        ) : (
          <>
            <GetUserPreferenceDialog {...{ userPreference, setUserPreference }} />
            {renderTeaser(teasers.slot1, { width: '300px', marginTop: '7px' })}
          </>
        ))}
    </Container>
  );
};

export default HomePage;
