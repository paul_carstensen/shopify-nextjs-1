#!/bin/bash

set -Eeuo pipefail

AWS_REGION=eu-central-1

STACK='shopify-nextjs-route53'

aws cloudformation deploy \
  --region $AWS_REGION \
  --stack-name $STACK \
  --template-file stacks/shopify-nextjs-route53-stack.yaml \
  --no-fail-on-empty-changeset

aws cloudformation update-termination-protection \
  --region $AWS_REGION \
  --enable-termination-protection \
  --stack-name $STACK
