#!/bin/bash

set -Eeuo pipefail

zip "shopify-nextjs-${VERSION}.zip" -r .
