#!/bin/bash

set -Eeuo pipefail

source ./bash_utils/stack.sh
source ./bash_utils/blue_green.sh
source ./bash_utils/ssm.sh
source ./bash_utils/params.sh

AWS_REGION=eu-central-1

check_environment_var

STACK="shopify-nextjs-ec2-$ENVIRONMENT"
AMI_ID=$(get_ssm node-ami-id)

vpc_output=$(stack_output VPC-$ENVIRONMENT)
vpc_id=$(param_from_stack_output "$vpc_output" VPC)
subnets=$(param_from_stack_output "$vpc_output" PrivateSubnets)

alb_output=$(stack_output shopify-nextjs-alb-$ENVIRONMENT)
alb_listener=$(param_from_stack_output "$alb_output" ListenerARN)
alb_sg=$(param_from_stack_output "$alb_output" ALBSecurityGroup)

bastion_output=$(stack_output bastion-$ENVIRONMENT)
bastion_sg=$(param_from_stack_output "$bastion_output" AccessibleByBastionSG)

role_output=$(stack_output shopify-nextjs-role-$ENVIRONMENT)
instance_profile=$(param_from_stack_output "$role_output" InstanceProfile)
ec2_role=$(param_from_stack_output "$role_output" Role)

logs_output=$(stack_output shopify-nextjs-logs-$ENVIRONMENT)
log_group=$(param_from_stack_output "$logs_output" LogGroup)

cognito_output=$(stack_output graphql-cognito-$ENVIRONMENT)
user_pool_client_id=$(param_from_stack_output "$cognito_output" UserPoolClientId)
user_pool_id=$(param_from_stack_output "$cognito_output" UserPoolId)
nextjs_cognito_username=$(get_ssm nextjs-cognito-username-$ENVIRONMENT)
nextjs_cognito_password=$(get_ssm nextjs-cognito-password-$ENVIRONMENT)

storefront_api_token=$(get_ssm shopify-nextjs-storefront-api-token)

bg_params=( $(get_deployment_parameters $STACK) )
current_bg="${bg_params[0]}"
new_bg="${bg_params[1]}"
instance_count="${bg_params[2]}"

new_stack="$STACK-$new_bg"

if [[ "$ENVIRONMENT" == 'prod' ]]; then
    site_url='https://www.uptodate.de'
else
    site_url='https://dev.app.uptodate.de'
fi

echo "Deploying stack $new_stack"

aws cloudformation deploy \
 --region $AWS_REGION \
 --stack-name $new_stack \
 --template-file stacks/shopify-nextjs-ec2-stack.yaml \
 --no-fail-on-empty-changeset \
 --parameter-overrides EnvironmentName=$ENVIRONMENT \
                       AMIId=$AMI_ID \
                       Subnets=$subnets \
                       VpcId=$vpc_id \
                       InstanceType=t3.medium \
                       ALBSecurityGroup=$alb_sg \
                       DesiredCapacity="$instance_count" \
                       InstanceProfile=$instance_profile \
                       Role=$ec2_role \
                       BastionAccessibleSG=$bastion_sg \
                       LogGroup=$log_group \
                       CognitoPoolClientId=$user_pool_client_id \
                       CognitoPoolId=$user_pool_id \
                       CognitoUsername=$nextjs_cognito_username \
                       CognitoPassword=$nextjs_cognito_password \
                       StorefrontAPIToken=$storefront_api_token \
                       Version=$VERSION \
                       SiteURL=$site_url \
|| (aws cloudformation delete-stack --region $AWS_REGION --stack-name $new_stack && exit 1)

app_output=$(stack_output $new_stack)
target_group=$(param_from_stack_output "$app_output" TargetGroupARN)

switch_bg "$STACK" "$alb_listener" "$target_group" "$current_bg"
