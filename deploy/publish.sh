#!/bin/bash

set -Eeuo pipefail

aws s3 cp "shopify-nextjs-${VERSION}.zip" "s3://uptodate-artifacts-eu-central-1/shopify-nextjs/"
