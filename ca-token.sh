#!/bin/bash

set -Eeuo pipefail

aws codeartifact get-authorization-token --region eu-central-1 --domain uptodate --domain-owner 951250281069 --query authorizationToken --output text > .codeartifact-token
