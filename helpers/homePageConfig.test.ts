import { sortedProducts, products, interestKeys } from './homePageConfig';

describe('homePageConfig', () => {
  it.each(interestKeys)('sorted products for interest "%s" should be exhaustive', (interest) => {
    expect(sortedProducts[interest]).toHaveLength(Object.keys(products).length);
  });
});
