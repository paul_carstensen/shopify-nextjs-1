import { IncomingMessage } from 'http';
import absoluteUrl from 'next-absolute-url';

const getAbsoluteUrl = (req: IncomingMessage, path: string | undefined = req.url): URL => {
  if (!path && path !== '') {
    throw new Error('no path specified or request path is unknown');
  }

  const baseUrl = absoluteUrl(req).origin;
  return new URL(path, baseUrl);
};

export default getAbsoluteUrl;
