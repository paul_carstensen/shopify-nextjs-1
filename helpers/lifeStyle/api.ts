import axios from 'axios';
import { Lifestyle, KeepMeUpdatedBody } from '../../types/risk/lifestyle';
import config from '../../config';

export const submitLifeStyles = async (
  lifeStyles: (string[] | string)[], consent: 0 | 1,
): Promise<Lifestyle[]> => (
  axios({
    method: 'POST',
    url: `${config().risk.apiUrl}/submit-lifestyles/${consent}`,
    data: lifeStyles,
  }).then((r) => r.data as Lifestyle[])
);

export const keepMeUpdated = async (
  keepMeUpdatedData: KeepMeUpdatedBody,
): Promise<boolean> => {
  await axios({
    method: 'POST',
    url: `${config().risk.apiUrl}/keep-me-updated`,
    data: JSON.stringify(keepMeUpdatedData),
  });
  return true;
};
