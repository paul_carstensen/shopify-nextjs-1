import axios from 'axios';
import { PageBlock } from '../../types/partnerPages';
import config from '../../config';

const fetchPageBlocks = async (pageId: number) => {
  const response = await axios({
    method: 'GET',
    url: config().partnerPages.graphqlUrl,
    params: {
      pageId,
    },
  });

  return response.data as PageBlock[];
};

export default fetchPageBlocks;
