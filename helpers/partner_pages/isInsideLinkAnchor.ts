const isInsideLinkAnchor = (buttonUrl: string): boolean => !!buttonUrl && buttonUrl.charAt(0) === '#';

export default isInsideLinkAnchor;
